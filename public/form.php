﻿<!DOCTYPE html>
<html lang="ru">
<head>
<title>Fromm</title>
<meta charset="utf-8">
<link rel="stylesheet" href="style.css" >
</head>
<body>
<style>
body{
	background-color:#C588F1;
}
.main-form{
    width:60%;
    display: flex;
    background-color:#B3B0EE;
	justify-content:center;
	border-radius:20px;	
	margin-left:20%;
}
form{
    text-align: center;
}
</style>

<div class ="main-form">
			
<div class = "text-field">
<form action="" method="POST">
<label>
Введите имя:<br />
<input type="text"
placeholder="name" name="fio">
</label><br />
<label>
Введите e-mail:<br />
<input type="email" name="email"
placeholder="e-mail">
</label><br />
<label>
Введите дату рождения:<br />
<input name="data" value="1917-11-07" type="date">
</label><br /><br>
Выберите пол: <br>
<label><input type="radio" name="pol" checked="checked" value="Девушка"> Девушка </label>
<label><input type="radio" name="pol" value="Мужчина"> Мужчина </label><br><br>
Количество конечностей:<br>
<input type="radio" name="konch" checked="checked"> 0
<label><input type="radio" name="konch" value="2"> 2 </label>
<label><input type="radio" name="konch" value="4"> 4 </label>
<label><input type="radio" name="konch" value="5"> 5 </label>
<label><input type="radio" name="konch" value="8"> 8 </label><br><br>
<label>
Выберите супер способности:<br>
<select name="sverh" multiple="multiple">
<option value="Бессмертие">бессмертие</option>
<option value="Прохождение сквозь стены">Прохождение сквозь стены</option>
<option value="Левитация">Левитация</option>
<option value="Fly">Fly</option>
<option value="Nichego">Nichego</option>
</select>
</label><br><br>
<label>
Биография
<br /><textarea id="main"
placeholder="Биография" name="bio"></textarea>
</label><br><br>
<label>
<input type="checkbox" name="chika">
 С контрактом ознакомлен/a
</label><br><br>
<input type="submit"
value="Отправить">
</form>
			</div>
		</div>
	</div>
	</body>
</html>